<?php
    /**
     * User: gleuton.pereira
     */

    /**
     * @param string $content
     * @param int $status
     * @param array $headers
     * @return \App\Http\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response|string
     */
    function service_response($content = '', int $status = 200, array $headers = [])
    {
        $factory = new \App\Http\ResponseFactory();

        if (func_num_args() === 0){
            return $factory;
        }

        return $factory->make($content, $status, $headers);
    }