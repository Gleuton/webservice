<?php
/**
 * User: gleuton
 * Date: 11/01/18
 */

namespace App\Http\Controllers;


use App\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function index()
    {
        return service_response(Client::all());
    }

    public function show($idClient)
    {
        $client = self::findClient($idClient);

        return service_response($client);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        $client = Client::create($request->all());

        return service_response($client,201);
    }

    public function update(Request $request, $idClient)
    {

        $client = self::findClient($idClient);

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        $client->fill($request->all());
        $client->save();
        return service_response($client,200);
    }

    public function destroy($idClient)
    {
        $client = self::findClient($idClient);

        $client->delete();

        return service_response('',204);
    }

    private function findClient($idClient)
    {
        if (!($client = Client::find($idClient))){
            throw new ModelNotFoundException('Client requisitado não existe');
        }

        return $client;
    }
}