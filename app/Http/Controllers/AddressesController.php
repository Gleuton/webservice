<?php
/**
 * User: gleuton
 * Date: 11/01/18
 */

namespace App\Http\Controllers;


use App\Address;
use App\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class AddressesController extends Controller
{
    public function index($clientId)
    {
        $client = self::findClient($clientId);

        return service_response($client->address);
    }

    public function show($clientId, $addressId)
    {
        $client = self::findClient($clientId);

        self::findAddress($addressId, $clientId);

        return service_response(
            $client->address->find($addressId)
        );
    }

    public function store(Request $request, $clientId)
    {
        $client = self::findClient($clientId);

        $this->validate($request, [
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required'
        ]);

        $address = $client->address()->create($request->all());

        return service_response($address,201);
    }

    public function update(Request $request, $clientId, $addressId)
    {

        $client = self::findClient($clientId);

        self::findAddress($addressId, $clientId);

        $this->validate($request, [
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required'
        ]);

        $address = $client->address->find($addressId)->fill($request->all());

        $address->save();

        return service_response($address,200);
    }

    public function destroy($clientId, $addressId)
    {
        $client = self::findClient($clientId);
        self::findAddress($addressId, $clientId);

        $client->address->find($addressId)->delete();

        return service_response('',204);
    }

    private function findClient($idClient)
    {
        if (!($client = Client::find($idClient))){
            throw new ModelNotFoundException('Client requisitado não existe');
        }

        return $client;
    }

    private function findAddress($addressId, $clientId)
    {
        if (!(Client::find($clientId)->address->find($addressId))){
            throw new ModelNotFoundException('Address requisitado não existe');
        }

        return Address::find($addressId);
    }
}