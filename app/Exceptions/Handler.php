<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class
    ];

    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

        if (in_array(get_class($e),$this->dontReport)){
            $response = parent::render($request, $e);
            $arrayError = [
                'status_code'   => $response->getStatusCode(),
                'message'       => $e->getMessage()
            ];

            if($e instanceof ValidationException){
                $arrayError['fields'] = $e->validator->getMessageBag()->toArray();
            }

            return service_response()->make(
                $arrayError,
                $response->getStatusCode()
            );
        }
        return parent::render($request, $e);
    }
}
