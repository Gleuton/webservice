<?php
    /**
     * Created by PhpStorm.
     * User: gleuton.pereira
     * Date: 23/01/2018
     * Time: 15:42
     */

    namespace App\Types;


    class ClientType
    {
        /**
         * @var string
         */
        public $name;
        /**
         * @var string
         */
        public $email;
        /**
         * @var string
         */
        public $phone;
    }